let elementToClickOn = document.querySelectorAll(".tabs-title");
let parentUl = document.querySelector(".tabs");
let allParagraphs = document.querySelectorAll('[data-show = "invisible"]');
allParagraphs.forEach(function (item){
    item.hidden = true;
});

parentUl.addEventListener("click", function (event){
    for (let element of elementToClickOn){
        element.classList.remove('active');
    }
    let targetLi = event.target;

    if (targetLi.hasAttribute("data-akali")){
        allParagraphs.forEach(function (item){
            item.hidden = true;
        })
        targetLi.classList.add('active');
        allParagraphs[0].hidden = false;

    } else if (targetLi.hasAttribute("data-anivia")){
        allParagraphs.forEach(function (item){
            item.hidden = true;

        })
        targetLi.classList.add('active');
        allParagraphs[1].hidden = false;

    } else if (targetLi.hasAttribute("data-draven")){
        allParagraphs.forEach(function (item){
            item.hidden = true;
        })
        targetLi.classList.add('active');
        allParagraphs[2].hidden = false;

    } else if (targetLi.hasAttribute("data-garen")){
        allParagraphs.forEach(function (item){
            item.hidden = true;
        })
        targetLi.classList.add('active');
        allParagraphs[3].hidden = false;

    } else if (targetLi.hasAttribute("data-katarina")){
        allParagraphs.forEach(function (item){
            item.hidden = true;
        })
        targetLi.classList.add('active');
        allParagraphs[4].hidden = false;
    }
});





